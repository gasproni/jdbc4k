import org.jetbrains.kotlin.gradle.dsl.KotlinJvmCompile
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

artifacts {
    add("archives", tasks["jar"])
}


allprojects {
    group = "com.asprotunity"
    version = "0.0.1"

    repositories {
        mavenCentral()
    }
}


plugins {
    kotlin("jvm") version "1.3.60"
    `maven-publish`
    signing
}


val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    testCompile(kotlin("test"))
    testCompile("org.junit.jupiter:junit-jupiter-api:5.4.0")
    testCompile("org.junit.jupiter:junit-jupiter-engine:5.4.0")
    testCompile("org.hamcrest:hamcrest:2.2")
    testCompile("org.mockito:mockito-junit-jupiter:3.1.0")
    testCompile("org.hsqldb:hsqldb:2.5.0")
    testCompile("org.junit.platform:junit-platform-launcher:1.4.0") {
        because("enables running in IntelliJ using JUnit runner")
    }
}

tasks {
    withType<KotlinJvmCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

    withType<Test> {
        useJUnitPlatform {
            includeEngines("junit-jupiter")
        }
    }

}

tasks.register<Jar>("sourcesJar") {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allJava)
}

tasks.register<Jar>("javadocJar") {
    archiveClassifier.set("javadoc")
    from(tasks.javadoc.get().destinationDir)
}


publishing {
    publications {

        create<MavenPublication>("mavenJava") {
            groupId = "com.asprotunity"
            artifactId = "jdbc4k"
            from(components["java"])
            artifact(tasks["sourcesJar"])
            artifact(tasks["javadocJar"])

            pom {
                name.set("Jdbc4k")
                description.set("A thin wrapper around JDBC to make it easier to use from Kotlin")
                url.set("http://jdbc4k.org")
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("gasproni")
                        name.set("Giovanni Asproni")
                        email.set("gasproni@asprotunity.com")
                    }
                }
                scm {
                    connection.set("scm:git:git@gitlab.com:gasproni/jdbc4k.git")
                    developerConnection.set("scm:git@gitlab.com:gasproni/jdbc4k.git")
                    url.set("http://jdbc4k.org")
                }
            }
        }
    }
    repositories {
        maven("https://oss.sonatype.org/service/local/staging/deploy/maven2/") {
            credentials {
                username = findProperty("sonatypeUsername") as String?
                password = findProperty("sonatypePassword") as String?
            }
        }
    }
}

signing {
    sign(publishing.publications["mavenJava"])
}
