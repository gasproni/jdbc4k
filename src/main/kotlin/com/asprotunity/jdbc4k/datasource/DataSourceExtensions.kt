package com.asprotunity.jdbc4k.datasource

import com.asprotunity.jdbc4k.connection.Connection
import com.asprotunity.jdbc4k.connection.internal.JDBC4KConnection
import javax.sql.DataSource

typealias Transaction<T> = Connection.() -> T


infix fun <T> DataSource.execute(transaction: Transaction<T>): T {
    return executeTransaction(this.connection, transaction)
}

fun <T> DataSource.executeAs(userName: String, password: String, transaction: Transaction<T>): T {
    return executeTransaction(getConnection(userName, password), transaction)
}

internal fun <T> executeTransaction(connection: java.sql.Connection, transaction: Transaction<T>): T {
    val dbc4kConnection = JDBC4KConnection(connection)
    try {
        val result = transaction(dbc4kConnection)
        dbc4kConnection.commit()
        return result
    } finally {
        dbc4kConnection.rollback()
        dbc4kConnection.close()
    }
}

