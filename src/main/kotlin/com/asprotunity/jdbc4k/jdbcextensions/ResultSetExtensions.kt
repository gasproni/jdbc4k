package com.asprotunity.jdbc4k.jdbcextensions

import java.io.InputStream
import java.io.Reader
import java.sql.Blob
import java.sql.Clob
import java.sql.ResultSet

fun ResultSet.getNullableInt(position: Int): Int? {
    val result = this.getInt(position)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableInt(columnName: String): Int? {
    val result = this.getInt(columnName)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableShort(position: Int): Short? {
    val result = this.getShort(position)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableShort(columnName: String): Short? {
    val result = this.getShort(columnName)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableLong(position: Int): Long? {
    val result = this.getLong(position)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableLong(columnName: String): Long? {
    val result = this.getLong(columnName)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableDouble(position: Int): Double? {
    val result = this.getDouble(position)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableDouble(columnName: String): Double? {
    val result = this.getDouble(columnName)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableFloat(position: Int): Float? {
    val result = this.getFloat(position)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableFloat(columnName: String): Float? {
    val result = this.getFloat(columnName)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableByte(position: Int): Byte? {
    val result = this.getByte(position)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableByte(columnName: String): Byte? {
    val result = this.getByte(columnName)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableBoolean(position: Int): Boolean? {
    val result = this.getBoolean(position)
    return valueOrNull(this, result)
}

fun ResultSet.getNullableBoolean(columnName: String): Boolean? {
    val result = this.getBoolean(columnName)
    return valueOrNull(this, result)
}

typealias BlobReaderFromStream<ResultType> = (InputStream?) -> ResultType

fun <ResultType : Any?> ResultSet.readBlob(
    position: Int,
    blobReader: BlobReaderFromStream<ResultType>
): ResultType {
    return fromBlob(this.getBlob(position), blobReader)
}

fun <ResultType : Any?> ResultSet.readBlob(
    columnName: String,
    blobReader: BlobReaderFromStream<ResultType>
): ResultType {
    return fromBlob(this.getBlob(columnName), blobReader)
}

typealias ClobReaderFromReader<ResultType> = (Reader?) -> ResultType

fun <ResultType : Any?> ResultSet.readClob(
    position: Int,
    clobReader: ClobReaderFromReader<ResultType>
): ResultType {
    return fromClob(this.getClob(position), clobReader)
}

fun <ResultType : Any?> ResultSet.readClob(
    columnName: String,
    clobReader: ClobReaderFromReader<ResultType>
): ResultType {
    return fromClob(this.getClob(columnName), clobReader)
}

fun <ResultType> fromBlob(
    blob: Blob?,
    readBlob: (InputStream?) -> ResultType
): ResultType {
    if (blob == null) {
        return readBlob(null)
    } else {
        try {
            blob.binaryStream.use { inputStream ->
                return readBlob(inputStream)
            }
        } finally {
            blob.free()
        }
    }
}

fun <ResultType> fromClob(
    clob: Clob?,
    readClob: (Reader?) -> ResultType
): ResultType {
    if (clob == null) {
        return readClob(null)
    } else {
        try {
            clob.characterStream.use { reader ->
                return readClob(reader)
            }
        } finally {
            clob.free()
        }
    }
}

internal fun <T> valueOrNull(rs: ResultSet, value: T): T? {
    if (rs.wasNull()) {
        return null
    }
    return value
}