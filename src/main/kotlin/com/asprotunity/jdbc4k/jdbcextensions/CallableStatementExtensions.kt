package com.asprotunity.jdbc4k.jdbcextensions

import java.sql.CallableStatement
import java.sql.Types

fun CallableStatement.getNullableInt(position: Int): Int? {
    val result = this.getInt(position)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableInt(columnName: String): Int? {
    val result = this.getInt(columnName)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableShort(position: Int): Short? {
    val result = this.getShort(position)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableShort(columnName: String): Short? {
    val result = this.getShort(columnName)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableLong(position: Int): Long? {
    val result = this.getLong(position)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableLong(columnName: String): Long? {
    val result = this.getLong(columnName)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableDouble(position: Int): Double? {
    val result = this.getDouble(position)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableDouble(columnName: String): Double? {
    val result = this.getDouble(columnName)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableFloat(position: Int): Float? {
    val result = this.getFloat(position)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableFloat(columnName: String): Float? {
    val result = this.getFloat(columnName)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableByte(position: Int): Byte? {
    val result = this.getByte(position)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableByte(columnName: String): Byte? {
    val result = this.getByte(columnName)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableBoolean(position: Int): Boolean? {
    val result = this.getBoolean(position)
    return valueOrNull(this, result)
}

fun CallableStatement.getNullableBoolean(columnName: String): Boolean? {
    val result = this.getBoolean(columnName)
    return valueOrNull(this, result)
}

fun CallableStatement.registerStringOutParameter(position: Int) {
    registerOutParameter(position, Types.LONGVARCHAR)
}

fun CallableStatement.registerIntOutParameter(position: Int) {
    registerOutParameter(position, Types.INTEGER)
}

fun CallableStatement.registerDoubleOutParameter(position: Int) {
    registerOutParameter(position, Types.DOUBLE)
}

fun CallableStatement.registerFloatOutParameter(position: Int) {
    registerOutParameter(position, Types.REAL)
}

fun CallableStatement.registerBooleanOutParameter(position: Int) {
    registerOutParameter(position, Types.BOOLEAN)
}

fun CallableStatement.registerByteOutParameter(position: Int) {
    registerOutParameter(position, Types.TINYINT)
}

fun CallableStatement.registerLongOutParameter(position: Int) {
    registerOutParameter(position, Types.BIGINT)
}

fun CallableStatement.registerShortOutParameter(position: Int) {
    registerOutParameter(position, Types.SMALLINT)
}

fun CallableStatement.registerDateOutParameter(position: Int) {
    registerOutParameter(position, Types.DATE)
}

fun CallableStatement.registerTimeOutParameter(position: Int) {
    registerOutParameter(position, Types.TIME)
}

fun CallableStatement.registerTimestampOutParameter(position: Int) {
    registerOutParameter(position, Types.TIMESTAMP)
}

internal fun <T> valueOrNull(cs: CallableStatement, value: T): T? {
    if (cs.wasNull()) {
        return null
    }
    return value
}