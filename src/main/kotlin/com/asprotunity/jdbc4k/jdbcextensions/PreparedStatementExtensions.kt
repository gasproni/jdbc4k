package com.asprotunity.jdbc4k.jdbcextensions

import java.io.InputStream
import java.io.Reader
import java.sql.PreparedStatement
import java.sql.Types

fun PreparedStatement.setNullableShort(position: Int, value: Short?) {
    this.setValue(position, value, Types.SMALLINT)
}

fun PreparedStatement.setNullableInt(position: Int, value: Int?) {
    this.setValue(position, value, Types.INTEGER)
}

fun PreparedStatement.setNullableLong(position: Int, value: Long?) {
    this.setValue(position, value, Types.BIGINT)
}

fun PreparedStatement.setNullableDouble(position: Int, value: Double?) {
    this.setValue(position, value, Types.DOUBLE)
}

fun PreparedStatement.setNullableFloat(position: Int, value: Float?) {
    this.setValue(position, value, Types.REAL)
}

fun PreparedStatement.setNullableByte(position: Int, value: Byte?) {
    this.setValue(position, value, Types.TINYINT)
}

fun PreparedStatement.setNullableBoolean(position: Int, value: Boolean?) {
    this.setValue(position, value, Types.BOOLEAN)
}

fun PreparedStatement.setNullableBlob(position: Int, inputStream: InputStream?) {
    if (inputStream == null) {
        this.setNull(position, Types.BLOB)
    } else {
        this.setBlob(position, inputStream)
    }
}

fun PreparedStatement.setNullableClob(position: Int, reader: Reader?) {
    if (reader == null) {
        this.setNull(position, Types.CLOB)
    } else {
        this.setClob(position, reader)
    }
}

internal fun PreparedStatement.setValue(position: Int, value: Any?, sqlType: Int) {
    this.setObject(position, value, sqlType)
}
