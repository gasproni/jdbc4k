package com.asprotunity.jdbc4k.streamextensions

import java.io.InputStream
import java.io.Reader
import java.nio.charset.Charset

fun InputStream.contentAsString(charset: Charset): String? {
    return java.util.Scanner(this, charset.name()).useDelimiter("\\A").next()
}

fun Reader.contentAsString(): String? {
    return java.util.Scanner(this).useDelimiter("\\A").next()
}