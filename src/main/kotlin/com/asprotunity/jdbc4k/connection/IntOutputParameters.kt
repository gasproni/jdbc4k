package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.getNullableInt
import com.asprotunity.jdbc4k.jdbcextensions.registerIntOutParameter
import com.asprotunity.jdbc4k.jdbcextensions.setNullableInt
import java.sql.CallableStatement

class IntOutputParameter : OutputParameterBase<Int>() {
    override fun readValue(statement: CallableStatement, position: Int): Int? {
        return statement.getNullableInt(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.registerIntOutParameter(position)
    }
}

class IntInputOutputParameter(inputValue: Int? = null) : InputOutputParameterBase<Int>(inputValue) {
    override fun readValue(statement: CallableStatement, position: Int): Int? {
        return statement.getNullableInt(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.setNullableInt(position, inputValue)
        statement.registerIntOutParameter(position)
    }
}
