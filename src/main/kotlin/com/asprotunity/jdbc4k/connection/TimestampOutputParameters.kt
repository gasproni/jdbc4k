package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.*
import java.sql.CallableStatement
import java.sql.Timestamp

class TimestampOutputParameter : OutputParameterBase<Timestamp>() {
    override fun readValue(statement: CallableStatement, position: Int): Timestamp? {
        return statement.getTimestamp(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.registerTimestampOutParameter(position)
    }
}


class TimestampInputOutputParameter(inputValue: Timestamp? = null) : InputOutputParameterBase<Timestamp>(inputValue) {
    override fun readValue(statement: CallableStatement, position: Int): Timestamp? {
        return statement.getTimestamp(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.setTimestamp(position, inputValue)
        statement.registerTimestampOutParameter(position)
    }
}


