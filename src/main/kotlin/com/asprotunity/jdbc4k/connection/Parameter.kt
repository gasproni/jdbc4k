package com.asprotunity.jdbc4k.connection

import java.sql.CallableStatement
import java.sql.PreparedStatement

typealias Parameter = (statement: CallableStatement, position: Int, scope: Scope) -> Unit

typealias InputParameter = (statement: PreparedStatement, position: Int, scope: Scope) -> Unit

class ParameterAlreadyBoundException(message: String) : Exception(message)
class ScopeNotClosedException(message: String) : Exception(message)


abstract class InputOutputParameter : Parameter {
    private var boundPosition: Int? = null

    final override operator fun invoke(statement: CallableStatement, position: Int, scope: Scope) {
        if (boundPosition != null) {
            throw ParameterAlreadyBoundException(
                "Output parameter at position ${position}" +
                        " already bound at position ${boundPosition}"
            )
        }
        boundPosition = position
        bind(statement, position, scope)
    }

    protected abstract fun bind(statement: CallableStatement, position: Int, scope: Scope)
}

abstract class OutputParameterBase<T> : InputOutputParameter() {
    var _value: T? = null
    var value: T?
        get() {
            if (scope == null || scope!!.isClosed) {
                return _value
            }
            throw ScopeNotClosedException("The scope is still open. Not all the results have been read yet!")
        }
        private set(value) {
            _value = value
        }

    private var scope: Scope? = null

    abstract fun registerOutParameter(statement: CallableStatement, position: Int)
    abstract fun readValue(statement: CallableStatement, position: Int): T?

    final override fun bind(statement: CallableStatement, position: Int, scope: Scope) {
        this.scope = scope
        registerOutParameter(statement, position)
        scope.add { this.value = readValue(statement, position) }
    }
}

abstract class InputOutputParameterBase<T>(val inputValue: T? = null) : OutputParameterBase<T>()