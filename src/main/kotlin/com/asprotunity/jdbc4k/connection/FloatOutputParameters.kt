package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.*
import java.sql.CallableStatement

class FloatOutputParameter : OutputParameterBase<Float>() {
    override fun readValue(statement: CallableStatement, position: Int): Float? {
        return statement.getNullableFloat(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.registerFloatOutParameter(position)
    }
}


class FloatInputOutputParameter(inputValue: Float? = null) : InputOutputParameterBase<Float>(inputValue) {
    override fun readValue(statement: CallableStatement, position: Int): Float? {
        return statement.getNullableFloat(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.setNullableFloat(position, inputValue)
        statement.registerFloatOutParameter(position)
    }
}
