package com.asprotunity.jdbc4k.connection

typealias RowMapperFun<T> = TableRow.() -> T

interface Connection {
    fun <T> select(
        sql: String,
        vararg parameters: InputParameter,
        rowMapper: RowMapperFun<T>
    ): Sequence<T>

    fun <T> call(
        sql: String,
        vararg parameters: Parameter,
        rowMapper: RowMapperFun<T>
    ): Sequence<T>

    fun call(sql: String, vararg parameters: Parameter)
    fun update(sql: String, vararg parameters: InputParameter): Int
    fun commit()
    fun rollback()
}
