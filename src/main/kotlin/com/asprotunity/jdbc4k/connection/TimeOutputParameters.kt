package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.*
import java.sql.CallableStatement
import java.sql.Time

class TimeOutputParameter : OutputParameterBase<Time>() {
    override fun readValue(statement: CallableStatement, position: Int): Time? {
        return statement.getTime(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.registerTimeOutParameter(position)
    }
}


class TimeInputOutputParameter(inputValue: Time? = null) : InputOutputParameterBase<Time>(inputValue) {
    override fun readValue(statement: CallableStatement, position: Int): Time? {
        return statement.getTime(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.setTime(position, inputValue)
        statement.registerTimeOutParameter(position)
    }
}


