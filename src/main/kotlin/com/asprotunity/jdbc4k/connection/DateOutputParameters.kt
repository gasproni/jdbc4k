package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.*
import java.sql.CallableStatement
import java.sql.Date

class DateOutputParameter : OutputParameterBase<Date>() {
    override fun readValue(statement: CallableStatement, position: Int): Date? {
        return statement.getDate(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.registerDateOutParameter(position)
    }
}


class DateInputOutputParameter(inputValue: Date? = null) : InputOutputParameterBase<Date>(inputValue) {
    override fun readValue(statement: CallableStatement, position: Int): Date? {
        return statement.getDate(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.setDate(position, inputValue)
        statement.registerDateOutParameter(position)
    }
}


