package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.*
import java.sql.CallableStatement

class DoubleOutputParameter : OutputParameterBase<Double>() {
    override fun readValue(statement: CallableStatement, position: Int): Double? {
        return statement.getNullableDouble(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.registerDoubleOutParameter(position)
    }
}

class DoubleInputOutputParameter(inputValue: Double? = null) : InputOutputParameterBase<Double>(inputValue) {
    override fun readValue(statement: CallableStatement, position: Int): Double? {
        return statement.getNullableDouble(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.setNullableDouble(position, inputValue)
        statement.registerDoubleOutParameter(position)
    }
}
