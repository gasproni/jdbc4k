package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.*
import java.sql.CallableStatement

class BooleanOutputParameter : OutputParameterBase<Boolean>() {
    override fun readValue(statement: CallableStatement, position: Int): Boolean? {
        return statement.getNullableBoolean(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.registerBooleanOutParameter(position)
    }
}


class BooleanInputOutputParameter(inputValue: Boolean? = null) : InputOutputParameterBase<Boolean>(inputValue) {
    override fun readValue(statement: CallableStatement, position: Int): Boolean? {
        return statement.getNullableBoolean(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.setNullableBoolean(position, inputValue)
        statement.registerBooleanOutParameter(position)
    }
}
