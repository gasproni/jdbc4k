package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.*
import java.io.InputStream
import java.io.Reader
import java.math.BigDecimal
import java.sql.Date
import java.sql.Time
import java.sql.Timestamp

fun bind(value: String?): InputParameter {
    return { statement, position, _ -> statement.setString(position, value) }
}


fun bind(value: Short?): InputParameter {
    return { statement, position, _ -> statement.setNullableShort(position, value) }
}

fun bind(value: Int?): InputParameter {
    return { statement, position, _ -> statement.setNullableInt(position, value) }
}

fun bind(value: Long?): InputParameter {
    return { statement, position, _ -> statement.setNullableLong(position, value) }
}

fun bind(value: Double?): InputParameter {
    return { statement, position, _ -> statement.setNullableDouble(position, value) }
}

fun bind(value: Float?): InputParameter {
    return { statement, position, _ -> statement.setNullableFloat(position, value) }
}

fun bind(value: Byte?): InputParameter {
    return { statement, position, _ -> statement.setNullableByte(position, value) }
}

fun bind(value: ByteArray): InputParameter {
    return { statement, position, _ -> statement.setBytes(position, value) }
}

fun bind(value: Boolean?): InputParameter {
    return { statement, position, _ -> statement.setNullableBoolean(position, value) }
}

fun bind(value: BigDecimal?): InputParameter {
    return { statement, position, _ -> statement.setBigDecimal(position, value) }
}

fun bind(value: Date?): InputParameter {
    return { statement, position, _ -> statement.setDate(position, value) }
}

fun bind(value: Time?): InputParameter {
    return { statement, position, _ -> statement.setTime(position, value) }
}

fun bind(value: Timestamp?): InputParameter {
    return { statement, position, _ -> statement.setTimestamp(position, value) }
}

fun bindBlob(provideStream: () -> InputStream?): InputParameter {
    return { statement, position, scope ->
        val inputStream = provideStream()
        if (inputStream != null) {
            scope.add(inputStream::close)
        }
        statement.setNullableBlob(position, inputStream)
    }
}

fun bindClob(provideReader: () -> Reader?): InputParameter {
    return { statement, position, scope ->
        val reader = provideReader()
        if (reader != null) {
            scope.add(reader::close)
        }
        statement.setNullableClob(position, reader)
    }
}

