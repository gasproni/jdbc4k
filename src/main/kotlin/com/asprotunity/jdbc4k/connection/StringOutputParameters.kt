package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.registerStringOutParameter
import java.sql.CallableStatement

class StringOutputParameter : OutputParameterBase<String>() {
    override fun readValue(statement: CallableStatement, position: Int): String? {
        return statement.getString(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.registerStringOutParameter(position)
    }
}

class StringInputOutputParameter(inputValue: String? = null) : InputOutputParameterBase<String>(inputValue) {
    override fun readValue(statement: CallableStatement, position: Int): String? {
        return statement.getString(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.setString(position, inputValue)
        statement.registerStringOutParameter(position)
    }
}
