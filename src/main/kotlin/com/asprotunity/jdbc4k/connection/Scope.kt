package com.asprotunity.jdbc4k.connection

import java.lang.RuntimeException

typealias BeforeCloseAction = () -> Unit

class ScopeException(exception: Exception) : RuntimeException(exception)

class Scope : AutoCloseable {

    val isClosed: Boolean
        get() { return beforeCloseActions.isEmpty() }

    val actionsCount: Int
    get() {
        return beforeCloseActions.size
    }

    private var beforeCloseActions = mutableListOf<BeforeCloseAction>()

    fun add(action: BeforeCloseAction) {
        beforeCloseActions.add(action)
    }

    fun <T> add(obj: T, closeFunction: T.() -> Unit): T {
        add { obj.closeFunction() }
        return obj
    }

    override fun close() {
        if (isClosed) {
            return
        }
        var toThrow: Exception? = null
        for (position in beforeCloseActions.size - 1 downTo 0) {
            try {
                beforeCloseActions[position]()
            } catch (exception: Exception) {
                if (toThrow == null) {
                    toThrow = ScopeException(exception)
                } else {
                    toThrow.addSuppressed(exception)
                }
            }
        }
        beforeCloseActions = mutableListOf()
        if (toThrow != null) {
            throw toThrow
        }
    }

}
