package com.asprotunity.jdbc4k.connection.internal

import com.asprotunity.jdbc4k.connection.*
import java.sql.CallableStatement
import java.sql.PreparedStatement


internal class JDBC4KTableRow(override val jdbcResultSet: java.sql.ResultSet) : TableRow

internal fun TableRow.next(): Boolean {
    return this.jdbcResultSet.next()
}

internal abstract class BoundStatement<T : PreparedStatement>(val statement: T) {

    val tableRow: TableRow
        get() {
            return JDBC4KTableRow(statement.resultSet)
        }

    val moreResults: Boolean
        get() = statement.moreResults

    val updateCount: Int
        get() = statement.updateCount

    fun close() {
        statement.close()
    }

    fun execute(): Boolean {
        return statement.execute()
    }

    abstract fun bindParameters(scope: Scope)

}

internal class PreparedBoundStatement(
    statement: PreparedStatement,
    private val parameters: Array<out InputParameter>
) :
    BoundStatement<PreparedStatement>(statement) {

    override fun bindParameters(scope: Scope) {
        bindParameters(parameters, statement, scope)
    }
}

internal class CallableBoundStatement(
    statement: CallableStatement,
    private val parameters: Array<out Parameter>
) :
    BoundStatement<CallableStatement>(statement) {

    override fun bindParameters(scope: Scope) {
        for ((position, param) in parameters.withIndex()) {
            param(statement, position + 1, scope)
        }
    }
}

internal class TableRowIterator(val tableRow: TableRow) : Iterator<TableRow> {
    private var lastCallWasHasNext = false
    var hasNext = false

    override fun hasNext(): Boolean {
        if (!lastCallWasHasNext) {
            hasNext = tableRow.next()
        }
        lastCallWasHasNext = true
        return hasNext
    }

    override fun next(): TableRow {
        if (!lastCallWasHasNext) {
            tableRow.next()
        }
        lastCallWasHasNext = false
        return tableRow
    }
}

internal object EmptyTableRowIterator : Iterator<TableRow> {
    override fun next(): TableRow {
        throw NotImplementedError()
    }

    override fun hasNext(): Boolean {
        return false
    }
}

internal class MultiTableRowsSequence<StetementType : PreparedStatement, ResultType>(
    private val statement: BoundStatement<StetementType>,
    private val rowMapper: RowMapperFun<ResultType>,
    transactionScope: Scope
) : Sequence<ResultType> {

    private var tableRowIterator: Iterator<TableRow> = EmptyTableRowIterator
    private val statementScope = transactionScope.add(Scope(), Scope::close)

    init {
        statementScope.add(statement::close)
        statement.bindParameters(statementScope)
        if (statement.execute()) {
            tableRowIterator = TableRowIterator(statement.tableRow)
        } else {
            nextTableRowIterator()
        }
    }

    fun nextTableRowIterator() {
        if (statementScope.isClosed) {
            tableRowIterator = EmptyTableRowIterator
            return
        }
        while (true) {
            if (statement.moreResults) {
                tableRowIterator = TableRowIterator(statement.tableRow)
                break
            } else if (statement.updateCount < 0) {
                statementScope.close()
                tableRowIterator = EmptyTableRowIterator
                break
            }
        }
    }

    override fun iterator(): Iterator<ResultType> {
        return object : Iterator<ResultType> {
            override fun next(): ResultType {
                return rowMapper(tableRowIterator.next())
            }

            override fun hasNext(): Boolean {
                if (!tableRowIterator.hasNext()) {
                    nextTableRowIterator()
                }
                return tableRowIterator.hasNext()
            }
        }
    }
}

internal class JDBC4KConnection(
    private val jdbcConnection: java.sql.Connection,
    private val transactionScope: Scope = Scope()
) : Connection {

    init {
        jdbcConnection.autoCommit = false
    }

    override fun <T> select(
        sql: String,
        vararg parameters: InputParameter,
        rowMapper: (TableRow) -> T
    ): Sequence<T> {
        return MultiTableRowsSequence(
            PreparedBoundStatement(jdbcConnection.prepareStatement(sql), parameters),
            rowMapper,
            transactionScope
        )
    }

    override fun <T> call(
        sql: String,
        vararg parameters: Parameter,
        rowMapper: RowMapperFun<T>
    ): Sequence<T> {
        return MultiTableRowsSequence(
            CallableBoundStatement(jdbcConnection.prepareCall(sql), parameters),
            rowMapper,
            transactionScope
        )
    }

    override fun call(sql: String, vararg parameters: Parameter) {
        // The call to toList forces all results to be read before returning
        // so that the out parameters can be read safely.
        call(sql, *parameters) {}.toList()
    }

    override fun update(sql: String, vararg parameters: InputParameter): Int {
        Scope().use { queryScope ->
            jdbcConnection.prepareStatement(sql).use {
                bindParameters(parameters, it, queryScope)
                return it.executeUpdate()
            }
        }
    }

    override fun commit() {
        transactionScope.close()
        jdbcConnection.commit()
    }

    override fun rollback() {
        transactionScope.close()
        jdbcConnection.rollback()
    }

    fun close() {
        transactionScope.close()
        jdbcConnection.close()
    }
}

internal fun bindParameters(
    parameters: Array<out InputParameter>,
    statement: PreparedStatement,
    scope: Scope
) {
    for ((position, param) in parameters.withIndex()) {
        param(statement, position + 1, scope)
    }
}


