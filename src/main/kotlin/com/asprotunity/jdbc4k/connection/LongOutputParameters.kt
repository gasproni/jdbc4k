package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.getNullableLong
import com.asprotunity.jdbc4k.jdbcextensions.registerLongOutParameter
import com.asprotunity.jdbc4k.jdbcextensions.setNullableLong
import java.sql.CallableStatement

class LongOutputParameter : OutputParameterBase<Long>() {
    override fun readValue(statement: CallableStatement, position: Int): Long? {
        return statement.getNullableLong(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.registerLongOutParameter(position)
    }
}

class LongInputOutputParameter(inputValue: Long? = null) : InputOutputParameterBase<Long>(inputValue) {
    override fun readValue(statement: CallableStatement, position: Int): Long? {
        return statement.getNullableLong(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.setNullableLong(position, inputValue)
        statement.registerLongOutParameter(position)
    }
}
