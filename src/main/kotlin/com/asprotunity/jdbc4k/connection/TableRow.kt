package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.*
import java.io.InputStream
import java.sql.Date
import java.sql.SQLException
import java.sql.Time
import java.sql.Timestamp

interface TableRow {

    val jdbcResultSet: java.sql.ResultSet

    val columnCount: Int
        get() = jdbcResultSet.metaData.columnCount

    /**
     * @param column the first column is 1, the second is 2, ...
     * @exception SQLException if a database access error occurs
     */
    fun columnName(column: Int): String {
        return jdbcResultSet.metaData.getColumnName(column)
    }

    /**
     * @param column the first column is 1, the second is 2, ...
     * @exception SQLException if a database access error occurs
     */
    fun columnLabel(column: Int): String {
        return jdbcResultSet.metaData.getColumnLabel(column)
    }

    /**
     * @param column the first column is 1, the second is 2, ...
     * @exception SQLException if a database access error occurs
     */
    fun tableName(column: Int): String {
        return jdbcResultSet.metaData.getTableName(column)
    }
}

fun TableRow.getString(position: Int): String? {
    return this.jdbcResultSet.getString(position)
}

fun TableRow.getString(columnName: String): String? {
    return this.jdbcResultSet.getString(columnName)
}

fun TableRow.getInt(position: Int): Int? {
    return this.jdbcResultSet.getNullableInt(position)
}

fun TableRow.getInt(columnName: String): Int? {
    return this.jdbcResultSet.getNullableInt(columnName)
}

fun TableRow.getShort(position: Int): Short? {
    return this.jdbcResultSet.getNullableShort(position)
}

fun TableRow.getShort(columnName: String): Short? {
    return this.jdbcResultSet.getNullableShort(columnName)
}

fun TableRow.getLong(position: Int): Long? {
    return this.jdbcResultSet.getNullableLong(position)
}

fun TableRow.getLong(columnName: String): Long? {
    return this.jdbcResultSet.getNullableLong(columnName)
}

fun TableRow.getDouble(position: Int): Double? {
    return this.jdbcResultSet.getNullableDouble(position)
}

fun TableRow.getDouble(columnName: String): Double? {
    return this.jdbcResultSet.getNullableDouble(columnName)
}

fun TableRow.getFloat(position: Int): Float? {
    return this.jdbcResultSet.getNullableFloat(position)
}

fun TableRow.getFloat(columnName: String): Float? {
    return this.jdbcResultSet.getNullableFloat(columnName)
}

fun TableRow.getByte(position: Int): Byte? {
    return this.jdbcResultSet.getNullableByte(position)
}

fun TableRow.getByte(columnName: String): Byte? {
    return this.jdbcResultSet.getNullableByte(columnName)
}

fun TableRow.getBoolean(position: Int): Boolean? {
    return this.jdbcResultSet.getNullableBoolean(position)
}

fun TableRow.getBoolean(columnName: String): Boolean? {
    return this.jdbcResultSet.getNullableBoolean(columnName)
}

fun TableRow.getDate(position: Int): Date? {
    return this.jdbcResultSet.getDate(position)
}

fun TableRow.getDate(columnName: String): Date? {
    return this.jdbcResultSet.getDate(columnName)
}

fun TableRow.getTime(columnName: String): Time? {
    return this.jdbcResultSet.getTime(columnName)
}

fun TableRow.getTime(position: Int): Time? {
    return this.jdbcResultSet.getTime(position)
}

fun TableRow.getTimestamp(columnName: String): Timestamp? {
    return this.jdbcResultSet.getTimestamp(columnName)
}

fun TableRow.getTimestamp(position: Int): Timestamp? {
    return this.jdbcResultSet.getTimestamp(position)
}

typealias BlobReaderFromStream<ResultType> = (InputStream?) -> ResultType

fun <ResultType : Any?> TableRow.readBlob(
    position: Int,
    blobReader: BlobReaderFromStream<ResultType>
): ResultType {
    return this.jdbcResultSet.readBlob(position, blobReader)
}

fun <ResultType : Any?> TableRow.readBlob(
    columnName: String,
    blobReader: BlobReaderFromStream<ResultType>
): ResultType {
    return this.jdbcResultSet.readBlob(columnName, blobReader)
}

fun <ResultType : Any?> TableRow.readClob(
    position: Int,
    clobReader: ClobReaderFromReader<ResultType>
): ResultType {
    return this.jdbcResultSet.readClob(position, clobReader)
}

fun <ResultType : Any?> TableRow.readClob(
    columnName: String,
    clobReader: ClobReaderFromReader<ResultType>
): ResultType {
    return this.jdbcResultSet.readClob(columnName, clobReader)
}
