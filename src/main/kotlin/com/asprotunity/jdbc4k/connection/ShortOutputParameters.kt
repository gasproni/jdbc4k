package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.getNullableShort
import com.asprotunity.jdbc4k.jdbcextensions.registerShortOutParameter
import com.asprotunity.jdbc4k.jdbcextensions.setNullableShort
import java.sql.CallableStatement

class ShortOutputParameter : OutputParameterBase<Short>() {
    override fun readValue(statement: CallableStatement, position: Int): Short? {
        return statement.getNullableShort(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.registerShortOutParameter(position)
    }
}

class ShortInputOutputParameter(inputValue: Short? = null) : InputOutputParameterBase<Short>(inputValue) {
    override fun readValue(statement: CallableStatement, position: Int): Short? {
        return statement.getNullableShort(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.setNullableShort(position, inputValue)
        statement.registerShortOutParameter(position)
    }
}

