package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.*
import java.sql.CallableStatement

class ByteOutputParameter : OutputParameterBase<Byte>() {
    override fun readValue(statement: CallableStatement, position: Int): Byte? {
        return statement.getNullableByte(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.registerByteOutParameter(position)
    }
}


class ByteInputOutputParameter(inputValue: Byte? = null) : InputOutputParameterBase<Byte>(inputValue) {
    override fun readValue(statement: CallableStatement, position: Int): Byte? {
        return statement.getNullableByte(position)
    }
    override fun registerOutParameter(statement: CallableStatement, position: Int) {
        statement.setNullableByte(position, inputValue)
        statement.registerByteOutParameter(position)
    }
}

