package queries

import com.asprotunity.jdbc4k.connection.*
import com.asprotunity.jdbc4k.datasource.execute
import com.asprotunity.jdbc4k.streamextensions.contentAsString
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.io.Reader
import java.io.StringReader
import java.nio.charset.Charset
import kotlin.test.assertEquals

class QueriesTest {

    companion object {
        private val dataSource = configureHSQLInMemoryDataSource()
    }

    @AfterEach
    fun after() {
        dropHSQLPublicSchema(dataSource)
    }

    @Test
    fun `does updates without bind values`() {

        dataSource execute {
            update("CREATE TABLE testtable (intvalue INTEGER NOT NULL)")
            update("INSERT INTO testtable (intvalue) VALUES (10)")
        }

        val found = dataSource execute {
            select("SELECT * FROM testtable") { getInt(1) }.toList()
        }

        assertEquals(1, found.size)
        assertEquals(10, found[0])
    }

    @Test
    fun `does updates with bind values`() {

        val expectedValues = Pair(10, "text")
        dataSource execute {
            update("CREATE TABLE testtable (intvalue INTEGER NOT NULL, textvalue VARCHAR(20) NOT NULL)")
            update(
                "INSERT INTO testtable (intvalue, textvalue) VALUES (?, ?)",
                bind(expectedValues.first), bind(expectedValues.second)
            )
        }

        val found = dataSource execute {
            select("SELECT * FROM testtable") {
                Pair(getInt(1), getString(2))
            }.toList()
        }

        assertEquals(1, found.size)
        assertEquals(expectedValues, found[0])
    }

    @Test
    fun `returned sequence can have null values`() {

        dataSource execute {
            update("CREATE TABLE testtable (intvalue INTEGER NULL)")
            update("INSERT INTO testtable (intvalue) VALUES (10)")
            update("INSERT INTO testtable (intvalue) VALUES (?)", bind(null as Int?))
        }

        val found = dataSource execute {
            select("SELECT * FROM testtable ORDER BY intvalue ASC") { getInt(1) }.toList()
        }

        assertEquals(2, found.size)
        assertEquals(null, found[0])
        assertEquals(10, found[1])
    }

    @Test
    fun `rolls back transaction when exception thrown`() {

        assertThrows<Exception> {
            dataSource execute {
                update("CREATE TABLE testtable (intvalue INTEGER NOT NULL)")
                update("INSERT INTO testtable (intvalue) VALUES (10)")
                throw Exception()
            }
        }

        val found = dataSource execute {
            select("SELECT * FROM testtable") {
                getInt("intvalue")
            }.toList()
        }
        assertEquals(0, found.size)

    }

    @Test
    fun `stores and reads blobs`() {
        val blobContent = "this is the content of the blob"
        val charset = Charset.forName("UTF-8")

        dataSource.execute {
            update("CREATE TABLE testtable (first BLOB NULL, second BLOB NULL)")
            update("INSERT INTO testtable (first, second) VALUES (?, ?)",
                bindBlob { null },
                bindBlob { ByteArrayInputStream(blobContent.toByteArray(charset)) }
            )
        }

        val blobReader = { inputStream: InputStream? -> inputStream?.contentAsString(charset) }

        val foundValues = dataSource.execute {
            select("SELECT * FROM testtable") {
                Pair(readBlob(1, blobReader), readBlob(2, blobReader))
            }.toList()
        }

        assertThat(foundValues.size, `is`(1))
        assertThat(foundValues[0].first, `is`(nullValue()))
        assertThat(foundValues[0].second, `is`(blobContent))

    }

    @Test
    fun `stores and reads clobs`() {
        val clobContent = "this is the content of the clob"

        dataSource.execute {
            update("CREATE TABLE testtable (first CLOB NULL, second CLOB NULL)")
            update("INSERT INTO testtable (first, second) VALUES (?, ?)",
                bindClob { null }, bindClob { StringReader(clobContent) }
            )
        }

        val clobReader = { reader: Reader? -> reader?.contentAsString() }

        val foundValues = dataSource.execute {
            select("SELECT * FROM testtable") {
                Pair(readClob(1, clobReader), readClob(2, clobReader))
            }.toList()
        }

        assertThat(foundValues.size, `is`(1))
        assertThat(foundValues[0].first, `is`(nullValue()))
        assertThat(foundValues[0].second, `is`(clobContent))
    }
}

