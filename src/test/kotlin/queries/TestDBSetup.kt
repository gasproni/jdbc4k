package queries

import com.asprotunity.jdbc4k.datasource.execute
import org.hsqldb.jdbc.JDBCDataSource
import javax.sql.DataSource

fun configureHSQLInMemoryDataSource(): DataSource {
    val result = JDBCDataSource()
    result.setUrl("jdbc:hsqldb:mem:testdb")
    result.user = "sa"
    result.setPassword("")
    return result
}

fun dropHSQLPublicSchema(dataSource: DataSource) {
    dataSource execute {
        update("DROP SCHEMA PUBLIC CASCADE")
    }
}