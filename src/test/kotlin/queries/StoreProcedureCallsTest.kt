package queries

import com.asprotunity.jdbc4k.connection.*
import com.asprotunity.jdbc4k.datasource.execute
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test


class StoreProcedureCallsTest {

    companion object {
        private val dataSource = configureHSQLInMemoryDataSource()
    }

    @AfterEach
    fun after() {
        dropHSQLPublicSchema(dataSource)
    }

    @Test
    fun `calls procedure with string input and output parameters`() {

        val expected = "expected value"
        dataSource execute {
            update(
                "CREATE PROCEDURE modify_out_param(IN inparam VARCHAR(${expected.length})," +
                        " OUT outparam VARCHAR(${expected.length}),\n" +
                        " INOUT ioparam VARCHAR(${expected.length}))\n" +
                        "BEGIN ATOMIC\n" +
                        " SET outparam = ioparam;\n" +
                        " SET ioparam = inparam;\n" +
                        "END"
            )
        }

        val outputParameter = StringOutputParameter()
        assertThat(outputParameter.value, `is`(nullValue()))

        val inputOutputParameter = StringInputOutputParameter("initial value")

        dataSource execute {
            call(
                "{call modify_out_param(?, ?, ?) }",
                bind(expected), outputParameter, inputOutputParameter
            )
        }

        assertThat(outputParameter.value, `is`("initial value"))
        assertThat(inputOutputParameter.value, `is`(expected))
    }


    @Test
    fun `calls procedure returning multiple result sets`() {

        dataSource execute {
            update("CREATE TABLE testtable1 (first VARCHAR(20) NULL, second INTEGER NOT NULL)")

            val intVal = 10
            update(
                "INSERT INTO testtable1 (first, second) VALUES (?, ?)",
                bind("ttable1val1"), bind(intVal)
            )
            update(
                "INSERT INTO testtable1 (first, second) VALUES (?, ?)",
                bind("ttable1val2"), bind(intVal + 1)
            )

            update("CREATE TABLE testtable2 (first2 VARCHAR(20) NULL)")
            update(
                "INSERT INTO testtable2 (first2) VALUES (?)",
                bind("ttable2val1")
            )
            update(
                "INSERT INTO testtable2 (first2) VALUES (?)",
                bind("ttable2val2")
            )
            update(
                "CREATE PROCEDURE multiple_results() MODIFIES SQL DATA\n" +
                        " DYNAMIC RESULT SETS 2\n" +
                        "BEGIN ATOMIC\n" +
                        " declare curs1 cursor for select first, second from testtable1;\n" +
                        " declare curs2 cursor for select first2 from testtable2;\n" +
                        " open curs1;\n" +
                        " INSERT INTO testtable1 (first, second) VALUES ('astring2', 1);\n" +
                        " open curs2;\n" +
                        "END"
            )
        }

        val expectedResults = listOf(
            Pair<String?, Int?>("ttable1val1", 10),
            Pair<String?, Int?>("ttable1val2", 11),
            Pair<String?, Int?>("ttable2val1", null),
            Pair<String?, Int?>("ttable2val2", null)
        )

        fun makeValue(rs: TableRow): Pair<String?, Int?> {
            if (rs.columnCount == 2) {
                return Pair(rs.getString("first"), rs.getInt("second"))
            } else {
                return Pair(rs.getString("first2"), null)
            }
        }

        val queryResults = dataSource execute {
            call("{call multiple_results() }") { makeValue(this) }.toList()
        }

        assertThat("", queryResults, `is`(expectedResults))
    }

    @Test
    fun `calls procedure returning empty result sets`() {

        dataSource execute {
            update("CREATE TABLE testtable1 (first VARCHAR(20) NULL, second INTEGER NOT NULL)")
            update("CREATE TABLE testtable2 (first2 VARCHAR(20) NULL)")

            update(
                "CREATE PROCEDURE multiple_results() MODIFIES SQL DATA\n" +
                        " DYNAMIC RESULT SETS 2\n" +
                        "BEGIN ATOMIC\n" +
                        " declare curs1 cursor for select first, second from testtable1;\n" +
                        " declare curs2 cursor for select first2 from testtable2;\n" +
                        " open curs1;\n" +
                        " open curs2;\n" +
                        "END"
            )
        }

        val expectedResults = listOf<String?>()

        val queryResults = dataSource execute {
            call("{call multiple_results() }") { getString(1) }.toList()
        }

        assertThat("", queryResults, `is`(expectedResults))
    }

    @Test
    fun `calls procedure returning with first result set empty and second with results`() {

        dataSource execute {
            update("CREATE TABLE testtable1 (first VARCHAR(20) NULL, second INTEGER NOT NULL)")
            update("CREATE TABLE testtable2 (first2 VARCHAR(20) NULL)")
            update(
                "INSERT INTO testtable2 (first2) VALUES (?)",
                bind("ttable2val1")
            )

            update(
                "CREATE PROCEDURE multiple_results() MODIFIES SQL DATA\n" +
                        " DYNAMIC RESULT SETS 2\n" +
                        "BEGIN ATOMIC\n" +
                        " declare curs1 cursor for select first, second from testtable1;\n" +
                        " declare curs2 cursor for select first2 from testtable2;\n" +
                        " open curs1;\n" +
                        " open curs2;\n" +
                        "END"
            )
        }

        val expectedResults = listOf<String?>("ttable2val1")

        val queryResults = dataSource execute {
            call("{call multiple_results() }") { getString(1) }.toList()
        }

        assertThat("", queryResults, `is`(expectedResults))
    }

    @Test
    fun `calls procedure returning results and output parameters`() {

        dataSource execute {
            update("CREATE TABLE testtable1 (first VARCHAR(20) NULL, second INTEGER NOT NULL)")
            val secondVal = 10
            update(
                "INSERT INTO testtable1 (first, second) VALUES (?, ?)",
                bind("ttable1val1"), bind(secondVal)
            )

            update(
                "CREATE PROCEDURE multiple_results_output_params(IN str VARCHAR(20), " +
                        "                                             OUT outnum INTEGER, " +
                        "                                             INOUT num INTEGER) MODIFIES SQL DATA\n" +
                        " DYNAMIC RESULT SETS 1\n" +
                        "BEGIN ATOMIC\n" +
                        " declare curs1 cursor for select first, second from testtable1;\n" +
                        " open curs1;\n" +
                        " INSERT INTO testtable1 (first, second) VALUES (str, num);\n" +
                        " SELECT second INTO num FROM testtable1 WHERE first = 'ttable1val1';\n" +
                        " SET outnum = num + 1;\n" +
                        "END"
            )
        }

        val ioNum = IntInputOutputParameter(15)
        val outNum = IntOutputParameter()

        val queryResults = dataSource execute {
            call(
                "{call multiple_results_output_params(?, ?, ?) }",
                bind("text"), outNum, ioNum
            ) {
                Pair(getString("first"), getInt("second"))
            }.toList()
        }

        assertThat(ioNum.value, `is`(10))
        assertThat(outNum.value, `is`(11))
        assertThat(queryResults, `is`(listOf(Pair<String?, Int?>("ttable1val1", 10))))
    }
}
