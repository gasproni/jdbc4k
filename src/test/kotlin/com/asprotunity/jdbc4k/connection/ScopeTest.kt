package com.asprotunity.jdbc4k.connection

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.*
import kotlin.test.assertEquals


class ScopeTest {


    @Test
    fun `scope is initially closed`() {
        val scope = Scope()
        assertTrue(scope.isClosed)
    }

    @Test
    fun `closes scope correctly when no handlers registered`() {
        val scope = Scope()
        scope.close()
        assertTrue(scope.isClosed)
    }

    @Test
    fun `calls single registered close handler`() {
        val closeable = mock(AutoCloseable::class.java)

        val scope = Scope()
        scope.add(closeable::close)
        scope.close()

        verify(closeable, times(1)).close()
        assertTrue(scope.isClosed)
    }

    @Test
    fun `close calls close on close handlers only once`() {
        val closeable = mock(AutoCloseable::class.java)

        val scope = Scope()
        scope.add(closeable::close)
        scope.close()
        scope.close()

        verify(closeable, times(1)).close()
        assertTrue(scope.isClosed)
    }

    @Test
    fun `calls registered close handlers in reverse registration order`() {
        val closeable1 = mock(AutoCloseable::class.java)
        val closeable2 = mock(AutoCloseable::class.java)

        val scope = Scope()
        scope.add(closeable1::close)
        scope.add(closeable2::close)
        scope.close()

        val order = inOrder(closeable1, closeable2)
        order.verify(closeable2, times(1)).close()
        order.verify(closeable1, times(1)).close()
        assertTrue(scope.isClosed)
    }

    @Test
    fun `closes normally and throws runtime exception when handler throws`() {
        val exceptionFromCloseable = Exception()
        val closeable = mock(AutoCloseable::class.java)
        doThrow(exceptionFromCloseable).`when`(closeable).close()

        val scope = Scope()
        scope.add(closeable::close)

        val exception = assertThrows<ScopeException> { scope.close() }

        assertTrue(scope.isClosed)
        assertEquals(exceptionFromCloseable, exception.cause)
    }

    @Test
    fun `calls all handlers and throws runtime exception wrapping first thrown with all others marked as suppressed when more than one handler throws`() {
        val closeable1 = mock(AutoCloseable::class.java)
        val closeable1Exception = Exception()
        doThrow(closeable1Exception).`when`(closeable1).close()

        val closeable2 = mock(AutoCloseable::class.java)

        val closeable3 = mock(AutoCloseable::class.java)
        val closeable3Exception = Exception()
        doThrow(closeable3Exception).`when`(closeable3).close()

        val scope = Scope()
        scope.add(closeable1::close)
        scope.add(closeable2::close)
        scope.add(closeable3::close)

        val exception = assertThrows<ScopeException> { scope.close() }

        assertTrue(scope.isClosed)
        assertEquals(closeable3Exception, exception.cause)
        assertEquals(1, exception.suppressed.size)
        assertEquals(closeable1Exception, exception.suppressed[0])
        verify(closeable1, times(1)).close()
        verify(closeable2, times(1)).close()
        verify(closeable3, times(1)).close()
    }

    @Test
    fun `before call actions list is emptied when scope is closed`() {
        val closeable1 = mock(AutoCloseable::class.java)
        val closeable2 = mock(AutoCloseable::class.java)
        val closeable3 = mock(AutoCloseable::class.java)

        val scope = Scope()
        scope.add(closeable1::close)
        scope.add(closeable2::close)
        scope.add(closeable3::close)

        assertEquals(3, scope.actionsCount)
        scope.close()
        assertEquals(0, scope.actionsCount)
    }

}