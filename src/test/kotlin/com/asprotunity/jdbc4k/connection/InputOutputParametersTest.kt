package com.asprotunity.jdbc4k.connection

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.mock
import java.sql.CallableStatement

class InputOutputParametersTest {

    @Test
    fun `throws exception if bound more than once`() {

        val ioParameter = object : InputOutputParameter() {
            override fun bind(statement: CallableStatement, position: Int, scope: Scope) {}
        }
        val scope = Scope()
        val statement = mock(CallableStatement::class.java)
        val firstPosition = 1
        val secondPosition = 2

        ioParameter(statement, firstPosition, scope)

        val exception = assertThrows<ParameterAlreadyBoundException> { ioParameter(statement, secondPosition, scope) }
        assertThat(
            exception.message, `is`(
                "Output parameter at position ${secondPosition}" +
                        " already bound at position ${firstPosition}"
            )
        )
    }

    @Test
    fun `output parameters values cannot be read if call scope is open`() {
        val outputParameter = IntOutputParameter()

        val scope = Scope()
        val statement = mock(CallableStatement::class.java)
        outputParameter(statement, 1, scope)

        val exception = assertThrows<ScopeNotClosedException> {  outputParameter.value }
        assertFalse(scope.isClosed)
        assertThat(exception.message, `is`("The scope is still open. Not all the results have been read yet!"))
    }
}


