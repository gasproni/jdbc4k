package com.asprotunity.jdbc4k.connection

import com.asprotunity.jdbc4k.jdbcextensions.setNullableBlob
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import java.io.InputStream
import java.io.Reader
import java.sql.PreparedStatement
import java.sql.Types

class InputParametersTest {

    val preparedStatement = mock<PreparedStatement>(PreparedStatement::class.java)
    val position = 1
    val scope = Scope()

    @Test
    fun `binds valid blobs`() {
        val blobStream: InputStream = mock(InputStream::class.java)

        bindBlob { blobStream }(preparedStatement, position, scope)

        verify(preparedStatement, times(1)).setNullableBlob(position, blobStream)
        verify(preparedStatement, times(1)).setBlob(position, blobStream)
        assertThat(this.scope.actionsCount, `is`(1))
        assertThatStreamIsClosedWhenScopeClosed(scope, blobStream)
    }

    @Test
    fun `binds null blobs`() {
        bindBlob { null }(preparedStatement, position, scope)
        verify(preparedStatement, times(1)).setNullableBlob(position, null)
        verify(preparedStatement, times(1)).setNull(position, Types.BLOB)
        assertThat(scope.actionsCount, `is`(0))
    }

    @Test
    fun `binds valid clobs`() {
        val clobReader = mock(Reader::class.java)
        bindClob { clobReader }(preparedStatement, position, scope)
        verify(preparedStatement, times(1)).setClob(position, clobReader)
        assertThat(this.scope.actionsCount, `is`(1))
        assertThatReaderIsClosedWhenScopeClosed(scope, clobReader)

    }

    @Test
    fun `binds null clobs`() {
        bindClob({ null })(preparedStatement, position, scope)
        verify(preparedStatement, times(1)).setNull(position, Types.CLOB)
        assertThat(scope.actionsCount, `is`(0))
    }

    private fun assertThatStreamIsClosedWhenScopeClosed(scope: Scope, blobStream: InputStream) {
        verify(blobStream, times(0)).close()
        scope.close()
        verify(blobStream, times(1)).close()
    }

    private fun assertThatReaderIsClosedWhenScopeClosed(scope: Scope, clobReader: Reader) {
        verify(clobReader, times(0)).close()
        scope.close()
        verify(clobReader, times(1)).close()
    }

}