package com.asprotunity.jdbc4k.datasource

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.*
import javax.sql.DataSource
import kotlin.test.assertEquals

class DataSourceExtensionsTest {

    @Test
    fun `executes transactions correctly`() {

        val jdbcStatement = mock(java.sql.PreparedStatement::class.java)
        `when`(jdbcStatement.execute()).thenReturn(false)
        `when`(jdbcStatement.moreResults).thenReturn(false)
        `when`(jdbcStatement.updateCount).thenReturn(-1)

        val jdbcConnection = mock(java.sql.Connection::class.java)
        val sql = "select * from ATable"
        `when`(jdbcConnection.prepareStatement(sql)).thenReturn(jdbcStatement)

        val dataSource = mock(DataSource::class.java)
        `when`(dataSource.connection).thenReturn(jdbcConnection)

        val result = dataSource execute { select(sql) { "doesn't matter" }; 10 }

        assertEquals(10, result)

        val order = inOrder(jdbcStatement, jdbcConnection, dataSource)

        order.verify(dataSource, times(1)).connection
        order.verify(jdbcStatement, times(1)).execute()
        order.verify(jdbcStatement, times(1)).close()
        order.verify(jdbcConnection, times(1)).commit()
        order.verify(jdbcConnection, times(1)).rollback()
        order.verify(jdbcConnection, times(1)).close()
    }

    @Test
    fun `rolls back without committing anything if exception is thrown`() {

        val jdbcStatement = mock(java.sql.PreparedStatement::class.java)
        val jdbcConnection = mock(java.sql.Connection::class.java)
        val query = "select * from ATable"
        `when`(jdbcConnection.prepareStatement(query)).thenReturn(jdbcStatement)

        val dataSource = mock(DataSource::class.java)
        `when`(dataSource.connection).thenReturn(jdbcConnection)

        assertThrows<Exception> { dataSource execute { throw Exception() } }

        val order = inOrder(jdbcStatement, jdbcConnection, dataSource)

        order.verify(dataSource, times(1)).connection
        order.verify(jdbcConnection, times(1)).rollback()
        order.verify(jdbcConnection, times(1)).close()

        verify(jdbcStatement, times(0)).close()
        verify(jdbcStatement, times(0)).executeQuery()
        verify(jdbcConnection, times(0)).commit()

    }

    @Test
    fun `transaction can return null results`() {
        val dataSource = mock(DataSource::class.java)
        val jdbcConnection = mock(java.sql.Connection::class.java)
        `when`(dataSource.connection).thenReturn(jdbcConnection)

        val result = dataSource execute {
            null as Int?
        }

        assertEquals(null, result)
    }
}